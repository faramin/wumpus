
%- Locations ----------------

/*
  wumpus([6, 6]).
  pit([[6, 6]]).
  gold([1, 4]).
*/

%- Dynamics -----------------

:- dynamic([
    marked/1,
    wumpus/1,
    pit/1,
    gold/1
  ]).

marked([[1, 1]]).

%----------------------------

start(Path) :- (
  retractall(wumpus(_)),
  retractall(gold(_)),
  retractall(pit(_)),
  retractall(marked(_)),
  assert(pit([])),

  write("wumpus position([X, Y] format): "),
  read(WP),
  assert(wumpus(WP)),

  write("pits positions([[X, Y], ...] format):"),
  read(PITS),
  assert(pit(PITS)),

  write("gold position([X, Y]): "),
  read(GP),
  assert(gold(GP)),

  can_walk(GP, Path),
  print_ans(Path)
).

print_ans([]).
print_ans([P|Rest]) :- (
  print(P),
  format("\n"),
  print_ans(Rest)
).

%----------------------------

can_walk(P, Path) :- (
  retractall(marked(_)),
  assert(marked([[1, 1]])),
  can_walk_mark(P, Path)
).

mark(P) :- (
  marked(Marks),
  append([P], Marks, NewMarks),
  retractall(marked(_)),
  assert(marked(NewMarks))
).

can_walk_mark([1, 1], [[1, 1]]).
can_walk_mark([X, Y], Path) :- (
  not(boom([X, Y])),
  not(wumpus([X, Y])),
  not(in_pit([X, Y])),

  marked(Marks),
  not(is_in([X, Y], Marks)),
  mark([X, Y]),

  inc(X, XI),
  dec(X, XD),
  inc(Y, YI),
  dec(Y, YD),

  (
    (
      can_walk_mark([XD, Y], PPath),
      append([[X, Y]], PPath, Path)
    );
    (
      can_walk_mark([XI, Y], PPath),
      append([[X, Y]], PPath, Path)
    );
    (
      can_walk_mark([X, YD], PPath),
      append([[X, Y]], PPath, Path)
    );
    (
      can_walk_mark([X, YI], PPath),
      append([[X, Y]], PPath, Path)
    )
  )
).

%----------------------------

boom([X, Y]) :- (
  X < 1; Y < 1; X > 4; Y > 4).

is_smelly(P) :- (
  wumpus(W),
  adj(P, W)).

is_bleezy_with(_, []) :- fail.
is_bleezy_with(P, [PP|Rest]) :- (
  adj(P, PP);
  is_bleezy_with(P, Rest)).

is_bleezy(P) :- (
  pit(PPs),
  is_bleezy_with(P, PPs)).

is_in(_, []) :- fail.
is_in(P, [PP|Rest]) :- (
  equal(P, PP);
  is_in(P, Rest)).

in_pit(P) :- (
  pit(PL),
  is_in(P, PL)).

%- Utils ---------------------

inc(X, Ans) :- sum_list([X, 1], Ans).
dec(X, Ans) :- sum_list([X, -1], Ans).

adj(1, 2).
adj(2, 1).
adj(2, 3).
adj(3, 2).
adj(3, 4).
adj(4, 3).

adj([X, Y], [U, V]) :- (
  (X = U, adj(Y, V));
  (Y = V, adj(X, Y))).

adj([X, Y1], [X, Y2]) :- adj(Y1, Y2).
adj([X1, Y], [X2, Y]) :- adj(X1, X2).
adj(_, _) :- fail.

equal(A, A).
equal(_, _) :- fail.

